import React from 'react';
import { Button, Row, Col, Card, Dropdown, Badge, Form, ToggleButtonGroup, ToggleButton } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { OverlayScrollbarsComponent } from 'overlayscrollbars-react';
import HtmlHead from 'components/html-head/HtmlHead';
import BreadcrumbList from 'components/breadcrumb-list/BreadcrumbList';
import CsLineIcons from 'cs-line-icons/CsLineIcons';
import ChartCustomHorizontalTooltip from 'views/interface/plugins/chart/ChartCustomHorizontalTooltip';
import ChartSmallLine1 from 'views/interface/plugins/chart/ChartSmallLine1';
import ChartSmallLine2 from 'views/interface/plugins/chart/ChartSmallLine2';
import ChartSmallLine3 from 'views/interface/plugins/chart/ChartSmallLine3';
import ChartSmallLine4 from 'views/interface/plugins/chart/ChartSmallLine4';
import ChartBubble from 'views/interface/plugins/chart/ChartBubble';
import ChartSmallDoughnutChart1 from 'views/interface/plugins/chart/ChartSmallDoughnutChart1';
import ChartSmallDoughnutChart2 from 'views/interface/plugins/chart/ChartSmallDoughnutChart2';
import ChartSmallDoughnutChart3 from 'views/interface/plugins/chart/ChartSmallDoughnutChart3';
import ChartSmallDoughnutChart4 from 'views/interface/plugins/chart/ChartSmallDoughnutChart4';
import ChartSmallDoughnutChart5 from 'views/interface/plugins/chart/ChartSmallDoughnutChart5';
import ChartSmallDoughnutChart6 from 'views/interface/plugins/chart/ChartSmallDoughnutChart6';

const DashboardsAnalytic = () => {
  const title = 'Resources';
  const description = 'Resources';

  const breadcrumbs = [
    { to: '', text: 'Home' },
    { to: 'dashboards', text: 'Dashboards' },
  ];

  return (
    <>
      <HtmlHead title={title} description={description} />
      

      

      

      <Row className="gy-5">
        {/* Consumptions Start */}
        <Col xl="12">
          
          <Card className="card w-100 sh-25 sh-sm-25">
            <img src="/img/banner/cta-wide-2.webp" className="card-img h-100" alt="card image" />
            <Row className="gy-5">
            <Col xl="6">
            <div className="card-img-overlay d-flex flex-column justify-content-between bg-transparent">
              
              <div className="mb-3 cta-3 text-primary">Search here!</div>
              <Row className="gx-2">
                <Col xs="12" sm="6">
                  <div className="d-flex flex-column justify-content-start">
                    <div className="text-muted mb-3 mb-sm-0">
                      <input type="email" className="form-control" placeholder="Search" />
                    </div>  
                  </div>
                </Col>
                <Col xs="12" sm="auto">
                  <Button variant="primary" className="btn-icon btn-icon-start">
                    <CsLineIcons icon="search" /> <span>Search</span>
                  </Button>
                </Col>
                <Col xs="12" sm="auto">
                  <Button variant="primary" className="btn-icon btn-icon-start">
                    <CsLineIcons icon="close" /> <span>Clear</span>
                  </Button>
                </Col>
              </Row>
              <br/>
              <Row className="gx-2">

                <ToggleButtonGroup type="radio" className="d-block" name="buttonOptions2">
                  <ToggleButton id="tbg-radio-3" value={3} variant="outline-primary">
                    Taxonomy
                  </ToggleButton>
                  <ToggleButton id="tbg-radio-4" value={4} variant="outline-primary">
                    Resources
                  </ToggleButton>
                  <ToggleButton id="tbg-radio-5" value={5} variant="outline-primary">
                    Categories
                  </ToggleButton>
                </ToggleButtonGroup>
              </Row>
              
            </div>
            </Col>
            </Row>
          </Card>
        </Col>
        {/* Consumptions End */}

        
      </Row>
    </>
  );
};

export default DashboardsAnalytic;
